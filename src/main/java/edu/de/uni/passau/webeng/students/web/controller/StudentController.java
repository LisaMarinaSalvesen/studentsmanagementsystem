package edu.de.uni.passau.webeng.students.web.controller;

import edu.de.uni.passau.webeng.students.application.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    // Todo Implement controller methods.
    // The Rest API mapping has to be done here.
    // No application logic should be implemented in the class. Call the Service for all application logic.
}
